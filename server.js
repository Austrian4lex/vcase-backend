const Koa = require('koa');
const router = require('koa-router')();
const _ = require('lodash');
const Promise = require('bluebird');
const log = require('./log');
const koaLogger = require('koa-bunyan');
const serve = require('koa-static');
const rewrite = require('koa-rewrite');
const send = require('koa-send');
const {
  baseURL,
  port,
  vgoURL,
  vgoAPIKey,
  affiliateAddress,
  sessionKeys,
  steamApiKey,
} = require('./config');
const Axios = require('axios');
const qs = require('qs');
const session = require('koa-session');
const passport = require('koa-passport');
const SteamStrategy = require('passport-steam');


const WebSocket = require('ws');
const http = require("http")
const url = require("url");



const app = new Koa();
var server = http.createServer(app.callback());

app.use(koaLogger(log, { level: 'info' }));

app.use(rewrite('/login/callback', '/'));
app.use(rewrite('/info', '/'));
router.get('/websocket', getWebsocketUrl)
router.get('/health', healthCheck);
router.get('/cases', getCases);
router.get('/keys', getKeyCount);
router.get('/keys/minopen', getMinOpenVolume)
router.post('/cases/:id/open', sendCaseOpenOffer);
router.get('/offer/:id', getCaseOpenOfferState);
router.get('/offers', getUserOffers);
router.get('/affiliate/stats', getAffiliateStats);
router.post('/affiliate/save', setUserAffiliate);
router.post('/affiliate/recruit', setRecruitCode);
router.get('/items', getItems);
router.get(
  '/auth',
  passport.authenticate('steam', {
    failureRedirect: '/',
    successRedirect: 'login/callback',
  })
);
router.get(
  '/auth/completed',
  passport.authenticate('steam', {
    failureRedirect: '/',
    successRedirect: 'login/callback',
  })
);
router.get('/auth/status', getAuthStatus);
router.delete('/auth', logout);

// everything for frontend
app.use(serve('public'));
router.get('*', sendFrontend);
async function sendFrontend(ctx) {
  await send(ctx, '/', { root: __dirname + '/public/index.html' });
}

const bodyParser = require('koa-bodyparser');
app.use(bodyParser());

require('koa-qs')(app);

app.keys = sessionKeys;
app.use(session({}, app));

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  done(null, obj);
});

app.use(passport.initialize());
app.use(passport.session());

passport.use(
  new SteamStrategy(
    {
      returnURL: `${baseURL}/auth/completed`,
      realm: `${baseURL}/`,
      apiKey: steamApiKey,
    },
    function(identifier, profile, done) {
      let user = {
        steamId: _.last(identifier.split('/')),
        name: profile._json.personaname,
        avatar: profile._json.avatar,
      };
      done(null, user);
    }
  )
);

app.use(async (ctx, next) => {
  try {
    await next();
  } catch (error) {
    // 500 handler
    log.error('Error handling request', error);
    ctx.body = {
      error,
    };
    ctx.status = 500;
  }
});

app.use(router.routes());

server.listen(port, () => {
  log.info(`VCase API listening on: ${port}`);
});


const vgoAPI = Axios.create({
  baseURL: vgoURL,
  headers: {
    Authorization: 'Basic ' + Buffer.from(vgoAPIKey + ':').toString('base64'),
  },
});



/* DATABASE */

const mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1:27017/vcase', { useNewUrlParser: true });

const DB = mongoose.model('userInfo', {
  steamId: String,
  affiliatedBy: { type: Number, default: null },
  affiliatedByCode: { type: String, default: "" },
  opskinsUserId: { type: Number, default: null },
  affiliateKey: { type: String, default: "" },
  affiliatedUser: { type: Number, default: 0 },
  affiliatedCases: { type: Number, default: 0 },
  affiliatedEarned: { type: Number, default: 0 },
  tradeOffers: [{
    offerId: Number,
    caseName: String,
    quantity: String,
    date: Number,
    status: String,
    link: String
  }],
  cases: [{
    offerId: Number,
    caseName: String,
    quantity: String,
    date: Number,
    status: String,
    openingLink: String,
    items: Array
  }],
  casesOpened: { type: Number, default: 0 }
});


const STATS = mongoose.model('stats', {
  id: {type: Number, default: 9000},
  totalCasesOpened: { type: Number, default: 0 },
  valueOfSkins: { type: Number, default: 0 }
});

STATS.findOne({id: 9000}, (error, data) => {
  if (!data) {
    const newStats = new STATS();
    newStats.save();
  }
});



async function healthCheck(ctx) {
  ctx.body = {
    api: 'up',
  };
  ctx.status = 200;
}

async function getCases(ctx) {
  let response = await vgoAPI.get('/ICase/GetCaseSchema/v1');
  ctx.body = response.data.response.cases;
  ctx.status = 200;
}

async function getKeyCount(ctx) {
  if (ctx.isUnauthenticated()) {
    ctx.status = 401;
    ctx.body = `{"error":"Unauthenticated"}`;
    return;
  }
  let response;
  try {
    response = await vgoAPI.get(
      `/ICaseSite/GetKeyCount/v1?${qs.stringify({
        steam_id: ctx.state.user.steamId,
      })}`
    );
    ctx.body = { keyCount: response.data.response.key_count };
    ctx.status = 200;
  } catch (error) {
    ctx.body = { message: error.response.data.message };
    ctx.status = error.response.status;
  }
}

async function sendCaseOpenOffer(ctx) {
  if (ctx.isUnauthenticated()) {
    ctx.status = 401;
    ctx.body = `{"error":"Unauthenticated"}`;
    return;
  }

  let caseId = ctx.params.id;
  let amount = ctx.request.body.amount;
  let steamId = ctx.state.user.steamId;
  let response;

  var object = await DB.findOne({ steamId: steamId }).exec();

  try {

    if (parseInt(object.affiliatedBy) > 0) {
      response = await vgoAPI.post('/ICaseSite/SendKeyRequest/v1', {
        steam_id: ctx.state.user.steamId,
        case_id: caseId,
        amount: amount,
        affiliate_eth_address: affiliateAddress,
        referral_uid: object.affiliatedBy
      });
    } else {
      response = await vgoAPI.post('/ICaseSite/SendKeyRequest/v1', {
        steam_id: ctx.state.user.steamId,
        case_id: caseId,
        amount: amount,
        affiliate_eth_address: affiliateAddress,
      });
    }

    ctx.body = {
      tradeId: response.data.response.offer.id,
      tradeOfferUrl: response.data.response.offer_url,
    };
      ctx.status = 200;
  } catch (error) {
    ctx.body = { message: error.response.data.message };
    ctx.status = error.response.status;
  }

}

async function getCaseOpenOfferState(ctx) {
  const OFFER_PENDING = 0;
  const OFFER_ACCEPTED = 1;
  const OFFER_FAILED = -1;
  const OPENING_PENDING = 0;
  const OPENING_COMPLETED = 1;
  const OPENING_FAILED = -1;
  const OPENING_PARTIAL_FAILURE = -2;

  let offerId = ctx.params.id;
  let response = await vgoAPI.get(
    `/ICaseSite/GetTradeStatus/v1?offer_id=${offerId}`
  );
  let items = _.chain(response.data.response.cases)
    .map(function(kase) {
      if (kase.item === null) {
        return null;
      }
      return {
        caseId: kase.id,
        name: kase.item.name,
        category: kase.item.category,
        image: kase.item.image,
        color: kase.item.color,
        suggested_price: kase.item.suggested_price,
        sku: kase.item.sku,
        wear: kase.item.wear,
        opened: false
      };
    })
    .compact();
  let offerState;
  switch (response.data.response.offer.state) {
    case 2:
      offerState = OFFER_PENDING;
      break;
    case 9:
    case 11:
    case 3:
      offerState = OFFER_ACCEPTED;
      break;
    default:
      offerState = OFFER_FAILED;
  }
  let openingState;
  let cases = response.data.response.cases;
  let itemsCount = response.data.response.offer.recipient.items.length;
  if (cases.length < itemsCount || cases.some(kase => kase.status === 2)) {
    openingState = OPENING_PENDING;
  } else if (cases.every(kase => kase.status === 3)) {
    openingState = OPENING_COMPLETED;
  } else if (cases.every(kase => kase.status === 1)) {
    openingState = OPENING_FAILED;
  } else {
    openingState = OPENING_PARTIAL_FAILURE;
  }
  ctx.body = {
    offerState: offerState,
    openingState: openingState,
    items: items,
    totalExpectedItems: itemsCount,
    time_created: response.data.response.offer.time_created,
    time_updated: response.data.response.offer.time_updated
  };
  ctx.status = 200;
}

async function getItems(ctx) {
  let needTrim = false;
  let skus = ctx.query.skus;

  let response = await vgoAPI.get(
    `/IItem/GetItems/v1?sku_filter=${skus}&wear_tier_index=1`
  );
  let items = _.map(response.data.response.items, function(item, sku) {
    if (item["1"] == undefined)
      return needTrim = true;

    let itemInfo = item["1"];
    return {
      name: itemInfo.name.replace(itemInfo.name.substring(itemInfo.name.length-13), ""),
      category: itemInfo.category,
      wearTier: itemInfo.wear_tier,
      image: itemInfo.image,
      color: itemInfo.color,
      rarity: itemInfo.rarity,
      suggested_price: (itemInfo.suggested_price / 100),
      sku: sku,
    };
  });
  if (needTrim)
    items.splice(0, 1);

  ctx.body = {
    items: items,
  };
  ctx.status = 200;
}

async function getUserOffers(ctx) {
  if (ctx.isUnauthenticated()) {
    ctx.status = 401;
    ctx.body = `{"error":"Unauthenticated"}`;
    return;
  }
  let response;
  let uid = ctx.query.uid;
  let state = ctx.query.state;
  let type =  ctx.query.type;

  try {
    response = await vgoAPI.get(
      `/ITrade/GetOffers/v1?uid=${uid}&state=${state}&type=${type}`
    );

    let offers = _.map(response.data.response.offers, function(offers) {
      return offers;
    });
    ctx.body = {
      offers: offers,
      total: response.data.response.total
    }
    ctx.status = 200;
  } catch (error) {
    ctx.body = { message: error.response.data.message };
    ctx.status = error.response.status;
  }

}

async function setUserAffiliate(ctx) {
  if (ctx.isUnauthenticated()) {
    ctx.status = 401;
    ctx.body = `{"error":"Unauthenticated"}`;
    return;
  }

  let steamId = ctx.state.user.steamId;
  let affiliatesCode = ctx.request.body.affiliatesCode;
  let wax_id = ctx.request.body.waxUserId;

  if (!affiliatesCode) {
    ctx.status = 200;
    ctx.body = `{"error":"No affiliatesCode provided!"}`;
    return;
  }

  if (!wax_id) {
    ctx.status = 200;
    ctx.body = `{"error":"No WAX ID provided!"}`;
    return;
  }

  var checkOPSkinsID = await DB.findOne({ opskinsUserId: wax_id }).exec();

  if (checkOPSkinsID) {
    ctx.body = {
      success: false,
      message: "Diese WAX User ID wird bereits mit dem Code: " + checkOPSkinsID.affiliateKey + " verwendet!"
    }
    ctx.status = 200;
    return;
  }

  var checkAffiliatesCode = await DB.findOne({ affiliateKey: affiliatesCode }).exec();

  if (checkAffiliatesCode) {
    ctx.body = {
      success: false,
      message: "Dieser Affiliates Code ist leider nicht mehr verfügbar!"
    }
    ctx.status = 200;
    return;
  }

  try {
    response = await vgoAPI.get(
      `/ICaseSite/UpdateCommissionSettings/v1?network_id=1&network_user_id=${wax_id}&referral_commission_rate=6.0`
    );

    if (response.data.response.network_user_id == wax_id) {
      await DB.findOneAndUpdate({"steamId": steamId}, {$set: {"opskinsUserId": wax_id, "affiliateKey": affiliatesCode }}).exec(function(error, data) {
        if (error) return console.log(error);
      });

      ctx.body = {
        success: true,
        message: "Du hast erfolgreich deinen Affiliates Code erstellt. Teile ihn nun mit der ganzen Welt (Code: "+affiliatesCode+")"
      };
      ctx.status = 200;
    }
  } catch (error) {
    ctx.body = { success: false, message: error.response.data.message };
    ctx.status = error.response.status;
  }

}


async function getAffiliateStats(ctx) {
  if (ctx.isUnauthenticated()) {
    ctx.status = 401;
    ctx.body = `{"error":"Unauthenticated"}`;
    return;
  }

  let steamId = ctx.state.user.steamId;

  // Users Stats
  var object = await DB.findOne({ steamId: steamId}).exec();

  ctx.body = {
    affiliatesCode: object.affiliateKey,
    affiliatedUser: object.affiliatedUser,
    affiliatedCases: object.affiliatedCases,
    affiliatedEarned: object.affiliatedEarned,
    affiliatedByCode: object.affiliatedByCode
  };

  ctx.status = 200;

}

async function setRecruitCode(ctx) {
  if (ctx.isUnauthenticated()) {
    ctx.status = 401;
    ctx.body = `{"error":"Unauthenticated"}`;
    return;
  }

  let steamId = ctx.state.user.steamId;
  let affiliatesCode = ctx.request.body.affiliatesCode;

  // Get UID from Key
  var userObject = await DB.findOne({"affiliateKey": affiliatesCode}).exec()

  if (!userObject) {
    ctx.body = {
      success: false,
      message : "Dieser Code ist nicht gültig!"
    }
    ctx.status = 200;
    return;
  }

  if (steamId == userObject.steamId) {
    ctx.body = {
      success: false,
      message : "Du kannst deinen eigenen Affiliates Code nicht für dich selbst nutzen!"
    }
    ctx.status = 200;
    return;
  }

  // Save it to user
  var newaffiliatedUser = userObject.affiliatedUser+=1;
  var test = await DB.findOneAndUpdate({"affiliateKey": affiliatesCode}, {$set: {"affiliatedUser": newaffiliatedUser }}).exec(function(error, data) {
    if (error) return console.log(error);
  });

  await DB.findOneAndUpdate({"steamId": steamId}, {$set: {"affiliatedBy": userObject.opskinsUserId, "affiliatedByCode": affiliatesCode }}).exec(function(error, data) {
    if (error) return console.log(error);
  });

  ctx.body = {
    success: true,
    message : "Du wurdest erfolgreich von dem Code: "+ affiliatesCode+ " rekrutiert!"
  }
  ctx.status = 200;

}

async function getUserInventory(ctx) {
  if (ctx.isUnauthenticated()) {
    ctx.status = 401;
    ctx.body = `{"error":"Unauthenticated"}`;
    return;
  }

  let response;
  let steamId =  ctx.query.steamId;

  try {
    response = await vgoAPI.get(
      `/ITrade/GetUserInventoryFromSteamId/v1?steam_id=${steamId}&app_id=1`
    );
    ctx.body = {
      total: response.data.response.total,
      items: response.data.response.items
    };
    ctx.status = 200;
  } catch (error) {
    ctx.body = { message: error.response.data.message };
    ctx.status = error.response.status;
  }

}

async function getAuthStatus(ctx) {
  if (ctx.isAuthenticated()) {

    var firstLogin = await DB.findOne({ steamId: ctx.state.user.steamId}).exec();
    if (!firstLogin) {
      const newUser = new DB({
        steamId: ctx.state.user.steamId
      });
      await newUser.save();
    }

    ctx.body = {
      authenticated: true,
      username: ctx.state.user.name,
      avatar: ctx.state.user.avatar,
      steamId: ctx.state.user.steamId,
    };
  } else {
    ctx.body = {
      authenticated: false,
      username: '',
      avatar: '',
      steamId: '',
    };
  }
  ctx.status = 200;
}

async function logout(ctx) {
  await ctx.logout();
  ctx.body = {};
  ctx.status = 200;
}


async function getMinOpenVolume(ctx) {
  let response = await vgoAPI.get(
    `/ICase/GetMinimumOpenVolume/v1`
  );
  ctx.body = {
    count: response.data.response.count
  };
  ctx.status = 200;
}

/*async function getWebsocketUrl(ctx) {
  if (ctx.isAuthenticated()) {
    ctx.body = {
      wsurl: "wss://vcase.site/wss/?username="+ctx.state.user.name+"&steamId="+ctx.state.user.steamId+"&avatar="+ctx.state.user.avatar
    };
  } else {
    ctx.body = {
      wsurl: "wss://vcase.site/wss/",
    };
  }
  ctx.status = 200;
}*/

async function getWebsocketUrl(ctx) {
  if (ctx.isAuthenticated()) {
    ctx.body = {
      wsurl: "ws://127.0.0.1:3001/?username="+ctx.state.user.name+"&steamId="+ctx.state.user.steamId+"&avatar="+ctx.state.user.avatar
    };
  } else {
    ctx.body = {
      wsurl: "ws://127.0.0.1:3001",
    };
  }
  ctx.status = 200;
}



var recentMessages = [];
var recentUnboxings = [];
var spinnerItems = {};
var caseImageStore = {};

var usersOnline = 0;
var totalCasesOpened = 0;
var skinsValue = 0;

setTimeout(() => {
  STATS.findOne({id: 9000}, (error, info) => {
    if (error) return console.log(error);

    totalCasesOpened = info.totalCasesOpened;
    skinsValue = info.valueOfSkins;
  });
}, 5000)


const wss = new WebSocket.Server({ server: server });

setInterval(() => {
  wss.clients.forEach((client) => {
    client.send(JSON.stringify({
      type:"stats",
      usersOnline: usersOnline,
      totalCasesOpened: totalCasesOpened,
      skinsValue: skinsValue
    }));
  });
}, 20 * 1000); // send ping to keep alive

wss.on('connection', function connection(ws, req) {

  ws.username= url.parse(req.url, true).query.username;
  ws.steamId= url.parse(req.url, true).query.steamId;
  ws.avatar= url.parse(req.url, true).query.avatar;

  usersOnline+=1;

  if (ws.username && ws.steamId && ws.avatar) {
    ws.send(JSON.stringify({ type: "alert", alert: 'success', message: 'Erfolgreich eingeloggt! Willkommen zurück ' + ws.username }))
    _getTradeOffers();
    _getListCasesToOpen();
  }

  ws.send(JSON.stringify({ type: "recentMessages", recentMessages }));
  ws.send(JSON.stringify({
    type:"stats",
    usersOnline: usersOnline,
    totalCasesOpened: totalCasesOpened,
    skinsValue: skinsValue
  }));

  _getUserLevel();

  ws.on('close', function close() {
    usersOnline-=1;
  });


  ws.on('message', function incoming(message) {

    message = JSON.parse(JSON.parse(message));

    if (message.type == "chat") {

      var date = new Date;

      if (ws.steamId == "76561198080283163") {
        var chatArray = {
          type: message.type,
          time: date.getTime(),
          status: "ti-crown",
          is_chat_mod: true,
          level: "∞",
          avatar: `${baseURL}/assets/img/logo.png`,
          username: "Admin",
          message: message.message
        };
      } else {
        var chatArray = {
          type: message.type,
          time: date.getTime(),
          status: "",
          is_chat_mod: false,
          level: ws.userLevel,
          avatar: ws.avatar,
          username: ws.username,
          message: message.message
        };

      }

      if (recentMessages.length < 30)
        recentMessages.push(chatArray);
      else {
        recentMessages.splice(0, -1);
        recentMessages.push(chatArray);
      }

      wss.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
          client.send(JSON.stringify(chatArray));
        }
      });

    }

    if (message.type == "getRecentUnboxings") {
      ws.send(JSON.stringify({ type: "recentUnboxings", recentUnboxings }));
    }

    if (message.type == "getUserLevel") {
      _getUserLevel();
    }

    if (message.type == "saveTradeOffer") {

      DB.findOne({ steamId: ws.steamId }, (err, dbentry) => {
        if (err) return console.log(err);

        if (dbentry) {
          newOffer = {
            offerId: message.offers.id,
            caseName: message.offers.caseName,
            quantity: message.offers.totalExpectedItems,
            date: message.offers.time_created,
            status: message.offers.state_name,
            link: message.offers.offerURL
          }

          newCases = {
            offerId: message.offers.id,
            caseName: message.offers.caseName,
            quantity: message.offers.totalExpectedItems,
            date: message.offers.time_created,
            status: "Warte"
          }
          // find by document id and update and push item in array
          DB.findByIdAndUpdate(dbentry._id,
              {$push: { tradeOffers: newOffer, cases: newCases }},
              { safe: true },
              function(err) { if (err) return console.log("Insert Error: " +err); }
          );
        } else {

          // create one time user in db
          const newTradeOffer = new DB({
            steamId: ws.steamId,
            tradeOffers: {
              offerId: message.offers.id,
              caseName: message.offers.caseName,
              quantity: message.offers.totalExpectedItems,
              date: message.offers.time_created,
              status: message.offers.state_name,
              link: message.offers.offerURL
            },
            cases: {
              offerId: message.offers.id,
              caseName: message.offers.caseName,
              quantity: message.offers.totalExpectedItems,
              date: message.offers.time_created,
              status: "Warte"
            }
          });
          newTradeOffer.save().then(() => console.log('Saved :: ' +ws.steamId));
        }


        setTimeout(() => {
          _getTradeOffers();
        }, 200)

      });
    }

    if (message.type == "saveCaseOpening") {

      if (message.status == "Abgelehnt") {
        DB.findOneAndUpdate({ steamId: ws.steamId },
            {$set: {"cases.$[i].status": message.status}},
            {arrayFilters: [{"i.offerId": parseInt(message.offerId)}], safe: true, overwrite: true},
            function(err) {
              if (err) return console.log("saveCaseOpening Error: " +err);
              setTimeout(() => {
                _getListCasesToOpen();
              }, 300)
            }
        );
      } else {
        DB.findOneAndUpdate({ steamId: ws.steamId },
            {$set: {"cases.$[i].items": message.items, "cases.$[i].openingLink": message.openingLink, "cases.$[i].status": message.status}},
            {arrayFilters: [{"i.offerId": parseInt(message.offerId)}], safe: true, overwrite: true},
            function(err) {
              if (err) return console.log("saveCaseOpening Error: " +err);
              setTimeout(() => {
                _getListCasesToOpen();
              }, 300)
            }
        );
      }

    }


    if (message.type == "updateTradeOffer") {
      DB.collection.findOneAndUpdate({'tradeOffers.offerId': message.offerId}, { $set: { 'tradeOffers.$.status': message.status}}, {new: true}, (err) => {
        if (err) return console.log(err);
        _getTradeOffers();
      });

    }


    if (message.type == "updateCaseOpeningStatus") {

      DB.collection.findOneAndUpdate({'cases.offerId': message.offerId}, { $set: { 'cases.$.status': message.status, 'cases.$.items': message.items }}, {new: true}, (err) => {
        if (err) return console.log(err);
        _getListCasesToOpen();
      });
    }


    if (message.type == "getListCaseOpening") {
      if (ws.readyState === WebSocket.OPEN)
        _getListCasesToOpen();
    }


    if (message.type == "getTradeOffers") {
      if (ws.readyState === WebSocket.OPEN)
        _getTradeOffers();
   }

   if (message.type == "getCasesToOpen") {
     _getCaseOpeningItems(message.offerId);
   }

   if (message.type == "openCase") {

     if (!ws.steamId) return;

     DB.collection.findOne({"steamId": ws.steamId},(err, info) => {
       if (err) return console.log(err);
       if (!info) return console.log("No Info!");

       DB.collection.findOne({ opskinsUserId: info.affiliatedBy}, (error, data) => {
          if (err) console.log(err);
          if (!data) return;
          // set new stats for affiliatesUser
          DB.findOneAndUpdate({ steamId: data.steamId} , {$set: {"affiliatedCases": data.affiliatedCases+=1, "affiliatedEarned": data.affiliatedEarned+=0.15 } }).exec((error) => {
             if (err) console.log(err);
          });
       });

       var casesOpened = parseInt(info.casesOpened);
       var newValue = casesOpened+=1;
       DB.collection.findOneAndUpdate({"steamId": ws.steamId},
         {$set: {"casesOpened": newValue}},
         (err) => {if (err) return console.log(err); }
       );
     });

     DB.collection.findOneAndUpdate(
       {"steamId": ws.steamId},
       {$set: {"cases.$[i].items.$[j].opened": true}},
       {arrayFilters: [{"i.offerId": parseInt(message.offerId)}, {"j.caseId": parseInt(message.caseId) }], new: true},
       (err, info) => {
         if (err) return console.log(err);

         for (var j = 0; j < info.value.cases.length; j++) {

           if (info.value.cases[j].offerId == message.offerId) {
             for (var k = 0; k < info.value.cases[j].items.length; k++) {
               if (info.value.cases[j].items[k].caseId == message.caseId) {

                 var date = new Date;

                 var unboxed = {
                   caseId: info.value.cases[j].items[k].caseId,
                   image: info.value.cases[j].items[k].image,
                   price: (info.value.cases[j].items[k].suggested_price / 100),
                   time: date.getTime(),
                   itemName: info.value.cases[j].items[k].name,
                   color: info.value.cases[j].items[k].color
                 }
                 if (recentUnboxings.length > 30)
                   recentUnboxings.splice(0, -1);

                 recentUnboxings.unshift(unboxed);

                 totalCasesOpened += 1;
                 skinsValue += unboxed.price;

                  setTimeout(() => {
                    wss.clients.forEach(function each(client) {
                      if (client.readyState === WebSocket.OPEN) {
                        client.send(JSON.stringify({type: "recentUnbox", unboxed}));
                        client.send(JSON.stringify({
                          type:"stats",
                          usersOnline: usersOnline,
                          totalCasesOpened: totalCasesOpened,
                          skinsValue: skinsValue
                        }));
                      }
                    });
                  }, 100);

                  STATS.findOneAndUpdate({id: 9000}, { $set: {"totalCasesOpened": totalCasesOpened, "valueOfSkins": skinsValue }}, (error) => {
                    if (error) console.log(error);
                  });

               }
             }
           }

         }

       });

       _getCaseOpeningItems(message.offerId);
       _updateCaseStatus(message.offerId);
       _getUserLevel();

   }

   if (message.type == "share_spinner_items") {

     spinnerItems[message.offerId] = [];
     spinnerItems[message.offerId] = message.spinner_items;
     caseImageStore[message.offerId] = [];
     caseImageStore[message.offerId] = message.caseImage

     wss.clients.forEach(function each(client) {
       if (client.readyState === WebSocket.OPEN) {
         client.send(JSON.stringify({
           type: "shared_spinner_items",
           offerId: message.offerId,
           spinner_items: message.spinner_items,
           caseImage: message.caseImage
         }));
       }
     });
   }

   if (message.type == "getSharedSpinnerItems") {
     ws.send(JSON.stringify({
       type: "shared_spinner_items",
       offerId: message.offerId,
       spinner_items: spinnerItems[message.offerId],
       caseImage: caseImageStore[message.offerId]
     }));
   }

   if (message.type == "startSharedSpin") {

     wss.clients.forEach(function each(client) {
       if (client.readyState === WebSocket.OPEN) {
         client.send(JSON.stringify({
           type: "startSharedSpin",
           offerId: message.offerId,
           spinner_items: message.spinner_items,
           i: message.i,
           a: message.a
         }));
       }
     });

   }

  });

  function _updateCaseStatus(offerId) {

    DB.findOne({ steamId: ws.steamId }, (err, dbentry) => {
     if (err) return console.log(err);

     var state = "";

     for (var i = 0; i < dbentry.cases.length; i++) {
       if (parseInt(dbentry.cases[i].offerId) == parseInt(offerId)) {
         var opened = 0;
         casesId = dbentry.cases[i]._id;
         for (var j = 0; j < dbentry.cases[i].items.length; j++) {
           if (!dbentry.cases[i].items[j].opened)
              opened++
         }

        if (opened > 0)
          state = (dbentry.cases[i].items.length-opened)+"/"+dbentry.cases[i].items.length+" Geöffnet";
        else
          state = "Geöffnet";
       }

     }

     DB.collection.findOneAndUpdate(
       {"steamId": ws.steamId},
       {$set: {"cases.$[i].status": state}},
       {arrayFilters: [{"i.offerId": parseInt(offerId)}] },
       (err) => { if (err) return console.log(err); }
     );

    });
  }


  function _getTradeOffers() {
    DB.findOne({ steamId: ws.steamId }, (err, dbentry) => {
      if (err)
        return console.log(err);

      if (dbentry)
        ws.send(JSON.stringify({
          type: "tradeOffers",
          offers: dbentry.tradeOffers
        }));
      else
        ws.send(JSON.stringify({
          type: "tradeOffers",
          offers: false
        }));
   });
  }

  function _getListCasesToOpen() {
    DB.findOne({ steamId: ws.steamId }, (err, dbentry) => {
      if (err)
        return console.log(err);

      if (dbentry.cases.length > 0)
        ws.send(JSON.stringify({
          type: "casesOpen",
          cases: dbentry.cases
        }));
      else
        ws.send(JSON.stringify({
          type: "casesOpen",
          cases: false
        }));
   });
  }

  function _getCaseOpeningItems(offerId) {
    DB.findOne({ "cases.offerId": offerId }, function (err, info) {
      if (err) return console.log(err);

      var owner;
      if (info.steamId == ws.steamId)
        owner = true
      else
        owner = false;

      info.cases.forEach(item => {
        if (item.offerId == offerId)
          ws.send(JSON.stringify({type: "casesToOpen", isOwner: owner, cases: item}));
      });
    });
  }

  function _getUserLevel() {
      DB.collection.findOne({"steamId": ws.steamId},(err, info) => {
        if (info)
          var casesOpened = parseInt(info.casesOpened);
        else
          var casesOpened = 0;

        var neededToLevelUP = 4;
        ws.userLevel = (1 + Math.floor(casesOpened/neededToLevelUP));
        ws.send(JSON.stringify({ type: "userLevel", level: ws.userLevel, progression: (casesOpened % neededToLevelUP) }));
      });
  }

});
